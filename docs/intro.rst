Download and Install
====================

Download
--------
Boxfish is available on the `PAVE <https://scalability.llnl.gov/performance-analysis-through-visualization/software.php>`_ website

Install
--------

Boxfish requires a Python 2.7, Qt 4.7+, numpy 1.6+, matplotlib 1.1.0+, PySide
1.0.1+, PyYaml and PyOpenGL. Ubuntu users may also need to install libgle3. 

To build the documentation, you need `Sphinx <http://sphinx-doc.org>`_.
